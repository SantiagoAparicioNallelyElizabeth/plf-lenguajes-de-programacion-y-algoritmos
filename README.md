# Lenguajes de programación y algoritmos
## Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)

```plantuml
@startwbs
skinparam backgroundColor #FFFFFF
*[#48005E] <font color=#FFFFFF><b>Programando ordenadores en los 80 y ahora

**[#732487] <font color=#FFFFFF><b>Programación
***[#C59FCB]< <font color=#48005E><b>Antes
****< Características
*****< Programas \neficientes
*****< Se aprovecha \nal máximo \nlos recursos
****> Limitaciones
*****> Dificultad \nen código \nútil 
*****> Se debe de \nconocer la \narquitectura
***[#C59FCB]> <font color=#48005E><b>Ahora
****< Ventajas 
*****< Programas más \nentendibles
*****< Sistemas más \ncomplejos
****> Desventajas 
*****> Se entrega \nun producto \nparcial
*****> Sobrecarga de \ncapas

**[#732487] <font color=#FFFFFF><b>Tipos de sistemas
***_ se clasifican en
****[#C59FCB]< <font color=#48005E><b>Antiguo
*****_ tiene
******< características   
*******_< son
********< No está en venta
********< Monolítico
********> Sin continuidad
*********_ ejemplo
********** Ordenadores de \nlos 80's 90's
****[#C59FCB]> <font color=#48005E><b>Actual
*****_> son
******< Sistemas de casa
******> Sistemas en tienda
*****_> existen
******> Diferencias
*******_> son
********< Potencia de \nlas máquinas
********> Cambios en su funcionamiento  

**[#732487] <font color=#FFFFFF><b>Lenguajes de programación
***_ se dividen en
****[#C59FCB]< <font color=#48005E><b>Ensamblador
*****_ usado en
******< Ordenadores \nantiguos
*******_ por ejemplo
********< MCX Consola \nde visión
****[#C59FCB]> <font color=#48005E><b>Alto nivel
*****_< usado en
******< Sistemas actuales
*****_> ejemplo
****** Java
*******_ se ejecuta
******** Entorno virtual
@endtwbs
```
## Hª de los algoritmos y de los lenguajes de programación (2010)
```plantuml
@startwbs
*[#48005E] <font color=#FFFFFF><b>Historia de los algoritmos y de los lenguajes de programación

**[#732487] <font color=#FFFFFF><b>Algoritmo
***_< se define como
**** Procedimiento sistemático \ny mecánico para \nresolver un problema
*****_ tiene
******[#C59FCB]< <font color=#48005E><b>Origen y evolución
*******_< mesopotamía
********< Inicio en los cálculos
*******_< Siglo XVII
********< Calculadoras mecánicas
*******_< Siglo XIX
********< Máquinas programables
*******_< Siglo XX
********< Máquinas similares a los actuales
******[#C59FCB]> <font color=#48005E><b>Características 
*******> Demasido antiguo
*******> Funciona como caja negra
*******> Soluciona problemas
***_> se clasifican en
****> Razonables
****_ se conoce como
***** Polinomiales
****> No razonables 
****_ se conoce como
***** Exponenciales
***** Super polinomiales

**[#732487] <font color=#FFFFFF><b>Programa
***_ se define como
**** Conjunto de operaciones \nen un lenguaje de programación \ny para una máquina en contreto
*****_ tiene una historia
******< 1800
*******_ se
******** Apreciaba en telares
******> Siglo XIX 
*******_ aparece en
******** La piola

**[#732487] <font color=#FFFFFF><b>Lenguaje de programación
***[#C59FCB]< <font color=#48005E><b>Antecedentes
****_< 1959
***** FORTRAN
****** Primer lenguaje \nde alto nivel
****_< 1960
***** Surge Lips
****** Representante del paradigma funcional
****_< 1968
***** Aparece Simula
****** Antecedente de lenguajes \norientados a objetos
****_< 1971
***** Aparece Prolog
****** Lenguaje basado en fórmulas lógicas
***_< se define como
**** Lenguaje formal
*****_< Se divide en
******< Iperativos
******< Declarativos
*******_ se tienen los
******** Funcionales
******** Lógicos
******< Orientado a objetos
***[#C59FCB]> <font color=#48005E><b>Funciones 
****_ como
*****> Escribir un \nconjunto de \n ordenes
*****> Crear programas \nque controlen \nel comportamiento \nfísico y lógico \nde una máquina
*****_> por ejemplo
****** C
****** C++
****** JAVA 

@endtwbs
```
## Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)
```plantuml
@startwbs
*[#48005E] <font color=#FFFFFF><b>Evolución de los lenguajes y paradigmas de programación

**[#732487] <font color=#FFFFFF><b>Lenguajes de programación
***_< se definen como
**** Medio de comunicación \nhombre-máquina
*****_ surge para
******< Manejar códigos \nnúmericos
***_> tiene a la
****[#C59FCB] <font color=#48005E><b>Programación estructurada
*****_ tiene
******< Características
*******_ las cuales son
********< Analiza una \nfunción principal
********< Descompone en \nfunciones más \nsencillas de resolver
******> Ejemplos
*******_ son
********> Basic
********> C
********> Pascal
********> Foltran
********> Java 

**[#732487] <font color=#FFFFFF><b>Paradigmas de programación
***_< se da por
**** Fallas de la \nprogramación \nestructurada
***_< ¿Qué es? 
**** Es una norma \nde pensar o \nabordar el \ndesarrollo de \nun programa.
***_> Tipos
****[#C59FCB]< <font color=#48005E><b>Funcional
*****_< se consideran
****** Programas verídicos
*******_ se usa en
********< Funciones matemáticas
********> Recursividad
*******_ ejemplo de lenguaje
********< ML
********> HOPE
********< HASKILL
********> ERLAN
****[#C59FCB]> <font color=#48005E><b>Lógico
*****_ tiene
******< Características
*******_< son
********< Uso de recursividad
********< Implica los valores \lógicos (cierto-falso)
********< No devuelve números, \nletras y operaciones \narímeticas
*******_> ejemplo de lenguaje
********> Prolog
********> Mercury
********> ALF
****[#C59FCB]< <font color=#48005E><b>Programación concurrente
*****_ se usa en 
****** Sistemas automáticos
*******_ tiene 
******** Características 
*********_< son
********** Sincronización 
********** Paralelismo \nentre tareas
*********_> ejemplo
**********< No se puede \nretirar de una \ncuenta vacía
**********> Transacciones \nsimultaneas en \nuna cuenta 
****[#C59FCB]> <font color=#48005E><b>Distributiva
*****_< características 
******< Comunicación entre ordenadores
******< Programas dirigidos
*****_< ejemplo 
******< Proyecto SETI
*****_> lenguaje 
******> ADA
******> E
******> Limbo
******> Oz
****[#C59FCB]> <font color=#48005E><b>Orientada a componentes
*****_< características 
******< Permite la reutilización
******> Conjunto de objetos
@endtwbs
```
